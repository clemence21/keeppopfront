import React, { useState, useEffect } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import UserService from "../services/user.service";

import AddArticle from "../components/NewsFeed.components/add-article.component";
import Article from "../components/NewsFeed.components/article.component";
import PublicAndPublishedArticlesList from "../components/NewsFeed.components/public_and_published_articles-list.component";


const Home = () => {
  const [content, setContent] = useState("");

  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        setContent(response.data);
        console.log(response.data);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, []);

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>{content}</h3>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/articles" className="navbar-brand">
          Newsfeed
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/articles"} className="nav-link">
              Articles
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/articles/create-article"} className="nav-link">
              Create article
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/articles"]} component={PublicAndPublishedArticlesList} />
          <Route exact path={"/articles/create-article"} component={AddArticle} />
          <Route path="/articles/:id" component={Article} />
        </Switch>
      </div>
      </header>
      <div>

    </div>

    </div>
  );
};

export default Home;