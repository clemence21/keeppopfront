import React, { useState } from "react";
import ArticleDataService from "../../services/article.service";

const AddArticle = () => {
    const initialArticleState = {
        id: null,
        title:"",
        description:"",
        category:"",
        is_published: 0,
        is_public: 0,
        is_comment_allowed: 0
    };

    const [article, setArticle] = useState(initialArticleState);
    const [submitted, setSubmitted] = useState(0);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setArticle({ ...article, [name]: value});
    };

    const saveArticle = () => {
        var data = {
            title: article.title,
            description: article.description,
            category: article.category
        };

        ArticleDataService.create(data)
        .then(response => {
            setArticle({
                id: response.data.id,
                title: response.data.title,
                description: response.data.description,
                is_published : response.data.is_published,
                is_public : response.data.is_public,
                is_comment_allowed : response.data.is_comment_allowed
            });
            setSubmitted(1);
            console.log(response.data)
        })
        .catch(error => {
            console.log(error);
        });
    };

    const newArticle = () => {
        setArticle(initialArticleState);
        setSubmitted(0);
    };
     

    return(
        <div className="submit-form">
        {submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={newArticle}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control"
                id="title"
                required
                value={article.title}
                onChange={handleInputChange}
                name="title"
              />
            </div>
  
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                required
                value={article.description}
                onChange={handleInputChange}
                name="description"
              />
            </div>
  
            <button onClick={saveArticle} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
};

export default AddArticle;